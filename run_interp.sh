#!/bin/sh

# import variables from paths.conf
. ./paths.conf

MYDIR=`dirname $0`
JYTHON_CLASSPATH=${JYTHON_PATH}/dist/jython.jar
TUPROLOG_CLASSPATH=${TUPROLOG_PATH}/src:${TUPROLOG_PATH}/lib/ikvm-api.jar:${TUPROLOG_PATH}/src:${TUPROLOG_PATH}/lib/mscorlib.jar:${TUPROLOG_PATH}/build/archives/2p.jar:${TUPROLOG_PATH}/build/archives/tuprolog.jar
EXTRA_CLASSPATH=.:${JYTHON_CLASSPATH}:${TUPROLOG_CLASSPATH}

# Embedded interpreter
JYTHONPATH=${MYDIR} java -classpath ${MYDIR}:${EXTRA_CLASSPATH} Main $@

# Using non-embedded
#JYTHONPATH=`pwd` CLASSPATH=${EXTRA_CLASSPATH} /opt/jython-ssl/dist/bin/jython $*
