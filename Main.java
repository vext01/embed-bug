import org.python.util.PythonInterpreter;
import org.python.util.InteractiveConsole;

public class Main {

    public static void main(String[] args) {
        if (args.length == 0) {
            // interactive
            InteractiveConsole interp = new InteractiveConsole();
            interp.interact();
        } else {
            // run a file
            // XXX this will not support for example './run_interp -m unittest'
            PythonInterpreter interp = new PythonInterpreter();
            PythonInterpreter.initialize(System.getProperties(), System.getProperties(), args);
            interp.execfile(args[0]);
        }
    }

}
