SRCS=			Main.java

JYTHON_CLASSPATH=	${JYTHON_PATH}/dist/jython.jar
EXTRA_CLASSPATH=	.:${JYTHON_CLASSPATH}

all:
	javac -classpath ${EXTRA_CLASSPATH} ${SRCS}

clean:
	rm Main.class

.include "paths.conf"
